#!/usr/bin/env node

const path = require("path");
const fs = require("fs");
const { ServiceBuilder } = require("selenium-webdriver/firefox");
const { Builder, By, until } = require("selenium-webdriver");

const geckoDriverPath = path.join(__dirname, "drivers/geckodriver");
const serviceBuilder = new ServiceBuilder(geckoDriverPath);

let driver;

/** PARAMETERS ***************/
const league = {
    code: "LJELCG6E",
    season: 2,
    games: 8,
    fixtures: 5,
};
const loginMail = "***********************";
const loginPassword = "*******************";
/****************************/

(async function example () {
    driver = await new Builder()
        .forBrowser("firefox")
        .setFirefoxService(serviceBuilder)
        .build();

    try {
        await login();
        let filename = "mbioligue_" + league.code + "_S" + league.season + ".json";
        fs.writeFile(filename, "", function () {
            console.log("*** New file created ***");
        });
        let completeJson = [];
        for (let gameWeek = 1; gameWeek <= league.games; gameWeek++) {
            for (let fixture = 1; fixture <= league.fixtures; fixture++) {
                let baseURL = "https://mpg.football/league/" + league.code + "/results/detail/";
                let completeUrl = baseURL + league.season + "_" + gameWeek + "_" + fixture;

                let json = {};
                json.url = completeUrl;
                console.log("Getting data from " + completeUrl);
                await driver.get(completeUrl);
                let fixtureJson = await getFixtureInfos(json);
                completeJson.push(fixtureJson);
            }
        }
        writeToFile(filename, completeJson);
    } catch (error) {
        console.log(error);
        process.exitCode = 1;
    } finally {
        await driver.quit();
    }
})();

async function login () {
    await driver.get("https://mpg.football");
    let connectFormButton = await getElement("button.jss73");
    await connectFormButton.click();
    let emailInput = await getElement("input[type='email']");
    await emailInput.sendKeys(loginMail);
    let passwordInput = await getElement("input[type='password']");
    await passwordInput.sendKeys(loginPassword);
    let formSubmit = await getElement("button[type='submit']");
    await formSubmit.click();
    await driver.get("https://mpg.football");
}

function writeToFile (filename, data) {
    fs.appendFile(filename, JSON.stringify(data) + "\n", function (err) {
        if (err) {
            console.log(err);
        }
    });
}

async function getFixtureInfos (json) {
    json.homeTeam = {};
    json.homeTeam.name = await getTextElement(".index__blocHome___2gJSm > .index__team___2teXs > .index__largeTitles___3-CrD");
    json.homeTeam.goals = await getTextElement(".index__scores___3Rbha .index__score___3hUb_:first-child .animated");
    json.awayTeam = {};
    json.awayTeam.name = await getTextElement(".index__blocAway___398Sh > .index__team___2teXs > .index__largeTitles___3-CrD");
    json.awayTeam.goals = await getTextElement(".index__scores___3Rbha .index__score___3hUb_:last-child .animated");
    let completeJson = await getTeamsInfos(json);
    return completeJson;
}

async function getTeamsInfos (json) {
    let players = await getElements(".pitch-top.pitch-horizontal li.player");
    let homePlayers = [];
    for (const index in players) {
        homePlayers.push(await getPlayerInfos(players[index]));
    }
    json.homeTeam.players = homePlayers;
    players = await getElements(".pitch-bottom.pitch-horizontal li.player");
    let awayPlayers = [];
    for (const index in players) {
        awayPlayers.push(await getPlayerInfos(players[index]));
    }
    json.awayTeam.players = awayPlayers;
    return json;
}

async function getPlayerInfos (player) {
    let playerInfos = {};
    playerInfos.name = await player.findElement(By.css("span.label")).getText();
    let playerNoteStr = await player.findElement(By.css("span.rate")).getText();
    playerInfos.note = Number(playerNoteStr.replace(",", "."));
    playerInfos.goals = {};
    let realGoals = await player.findElements(By.css("svg.goal:not(.mpg):not(.csc):not(.cancel)"));
    playerInfos.goals.real = realGoals.length;
    let mpgGoals = await player.findElements(By.css("svg.goal.mpg"));
    playerInfos.goals.mpg = mpgGoals.length;
    let cscGoals = await player.findElements(By.css("svg.goal.csc"));
    playerInfos.goals.csc = cscGoals.length;
    let cancelGoals = await player.findElements(By.css("svg.goal.cancel"));
    playerInfos.goals.cancel = cancelGoals.length;
    return playerInfos;
}

async function getElement (cssSelector) {
    await driver.wait(until.elementLocated(By.css(cssSelector)), 10000, "Element " + cssSelector + " non trouvé");
    const element = await driver.findElement(By.css(cssSelector));
    await driver.wait(until.elementIsVisible(element), 10000, "Element " + cssSelector + " non visible");
    return element;
}
async function getElements (cssSelector) {
    await driver.wait(until.elementLocated(By.css(cssSelector)), 10000, "Element " + cssSelector + " non trouvé");
    const elements = await driver.findElements(By.css(cssSelector));
    return elements;
}

async function getTextElement (cssSelector) {
    let element = await getElement(cssSelector);
    let elementText = await element.getText();
    return elementText;
}

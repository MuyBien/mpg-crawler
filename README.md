## Permet de récupérer les informations de ligues MPG dans un fichier JSON

Sont récupéré pour chaque match :
* les noms des équipes
* le score
* les joueurs avec leur note et leurs buts (réels, MPG, CSC et annulés par valise)

module.exports = {
    root: true,
    extends: [
        "eslint:recommended",
    ],
    env: {
        node: true,
        es6: true
    },
    rules: {
        indent: ["error", 4 ],
        quotes: ["error", "double" ],
        semi: ["error", "always" ],
        "comma-dangle": ["error", "always-multiline"],
        curly: "error",
        "no-multi-spaces": "error",
        "quote-props": ["error", "as-needed" ],
        "no-console": "off",
        "no-trailing-spaces": "error",
        "space-before-function-paren": "error",
        "semi-spacing": "error",
        "space-infix-ops": "error",
        "keyword-spacing": ["error", {
            before: true,
            after: true,
        }],
    },
    "parserOptions": {
        "ecmaVersion": 8
    },
};
